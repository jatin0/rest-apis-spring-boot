package com.example.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.api.model.userDetails;
import com.example.api.service.userServices;

@RestController
@RequestMapping("/user")
public class userController {
	
	@Autowired
	private userServices service;
	
	@PostMapping("/userDetails")
	public userDetails createUser(@Valid @RequestBody userDetails users) {
		return service.saveUserDetails(users);
	}
	
	@GetMapping("/userDetails")
	public List<userDetails> getallusers() {
		return service.allUser();
	}	
	
	@GetMapping("/userDetails/{id}")
	public Optional<userDetails> getUserById(@PathVariable(value = "id") Long userId){
		return service.userById(userId);
	}
	
	@PutMapping("/userDetails")
	public String updateUserById(@Valid @RequestBody userDetails users){
		return service.updateUser(users);
	}
	
	@DeleteMapping("/userDetails/{id}")
	public String deleteUserById(@PathVariable(value = "id") Long userId){
		return service.deleteUser(userId);
	}
}
