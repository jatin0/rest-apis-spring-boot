package com.example.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.model.userDetails;
import com.example.api.model.repository.userRepository;


@Service
public class userServices {
	
	@Autowired
	userRepository userRepo;
	
	public userDetails saveUserDetails(userDetails user) {
		return userRepo.save(user);
	}
	
	public List<userDetails> allUser() {
		List<userDetails> users = new ArrayList<>();
		userRepo.findAll().forEach(users :: add);
		return users;
	}
	
	public Optional<userDetails> userById(Long id) {
		return userRepo.findById(id);
	}
	
	public String updateUser(userDetails user) {
		userRepo.saveAndFlush(user);
		return "Data Updated";
	}
	
	public String deleteUser(Long id) {
		userRepo.deleteById(id);
		return "Data of id " +id+ " Delete";
	}
}
