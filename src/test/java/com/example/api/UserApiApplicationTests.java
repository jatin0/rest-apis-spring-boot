package com.example.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.api.model.userDetails;
import com.example.api.model.repository.userRepository;
import com.example.api.service.userServices;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserApiApplicationTests {

	@Autowired
	public userServices service;
	
	@MockBean
	public userRepository userRepo;
	
	@Test
	public void getUserTest() {
		List<userDetails> toDoList = new ArrayList<userDetails>();
		toDoList.add(new userDetails("vivek","bihar"));
		toDoList.add(new userDetails("aman","MP"));
		toDoList.add(new userDetails("yatin","punjab"));
		when(userRepo.findAll()).thenReturn(toDoList);
		
		assertEquals(3, service.allUser().size());
		
//		when(userRepo.findAll()).thenReturn(Stream.of(new userDetails("vikash", "mumbai")).collect(Collectors.toList()));
//		assertEquals(1, service.allUser().size());
	}
	
	@Test
	public void createUserTest() {
		userDetails userList = new userDetails(15,"rahul","bihar");
		when(userRepo.save(userList)).thenReturn(userList);
		userDetails res = service.saveUserDetails(userList);
		
		assertEquals(userList, res);
	}
	
	@Test
	public void updateUserTest() {
		userDetails userList = new userDetails(9,"anand","bihar");
		when(userRepo.save(userList)).thenReturn(userList);
		userDetails updateUser = new userDetails(9,"manish","Delhi");
		when(userRepo.saveAndFlush(updateUser)).thenReturn(updateUser);
		
		assertEquals("manish", updateUser.getName());
	}
	
	@Test
	public void deleteUserTest() {
		userDetails userList = new userDetails(19, "ram", "Model Town");
		when(userRepo.save(userList)).thenReturn(userList);
		service.deleteUser(userList.getId());
        verify(userRepo, times(1)).deleteById(userList.getId());
	}
	
	@Test 
	public void getUserByIdTest() {
		Optional<userDetails> userList = Optional.of(new userDetails(30, "raju", "Rohini"));
		Long id = userList.get().getId();
		when(userRepo.findById(id)).thenReturn(userList);
		
		assertEquals(id, service.userById(id).get().getId());
	}

}
